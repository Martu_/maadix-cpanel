# python
from re import compile
from socket import error as socket_error
# django
from django.http import HttpResponseRedirect,HttpResponse
from django.urls import reverse
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import PermissionDenied
from django.utils.text import slugify
# contrib
from ldap3.core.exceptions import LDAPSocketOpenError
# project
from django.conf import settings
from . import utils

class LdapConnectionMiddleware:
    """
    Middleware that requires a user to be authenticated to view any page other
    than LOGIN_URL. Exemptions to this requirement can optionally be specified
    in settings via a list of regular expressions in LOGIN_EXEMPT_URLS (which
    you can copy from your urls.py).
    """

    def __init__(self, get_response):
        self.get_response = get_response
        # Create list of urls that are allowed for the different roles
        # [we compile urls as regex expressions to allow url patterns in ALLOWED_URLS if needed]
        self.ALLOWED_URLS = { k:[compile(e) for e in v] for k,v in settings.ALLOWED_URLS.items() }

    def __call__(self, request):
        # get current path
        path = request.path_info.lstrip('/')
        # In any logged-in view try connecting ldap
        if not any(regex.match(path) for regex in self.ALLOWED_URLS['anonymous']):
            if (request.COOKIES.get('s') and 'user' in request.session):
                try:
                    username = request.session['user']
                    password = utils.dec_pwd(request)
                    role     = utils.get_user_role(username)
                    ldap     = utils.connect_ldap(username, password)
                    # if connection, check permissions and set ldap as a property of
                    # the request, available to any view
                    if ldap['connection']:
                        # Check permissions
                        if not any(regex.match(path) for regex in self.ALLOWED_URLS[role]):
                            if role == 'anonymous':
                                messages.success(request, _('Has de estar logueado para ver esa página'))
                                return HttpResponseRedirect( reverse('login') )
                            raise PermissionDenied
                        # set session parameters as request attributes
                        request.user.role     = role
                        request.user.username = username
                        setattr(request, "ldap", ldap['connection'])

                        # If role is admin get ldap status
                        # Part of this status will be passed to global context (menu, notifications)
                        # through context_processors.py
                        if role == 'admin':
                            # Check if cpanel is activated. If not redirect to activation page
                            request.ldap.search(
                                search_base   = settings.LDAP_TREE_BASE ,
                                search_filter = "(&(objectClass=extensibleObject)(cn=%s))" % username,
                                attributes    = ['status']
                            )
                            admin_status = request.ldap.entries[0].status.value
                            # Redirect only if page is not activate - avoid infinite redirection
                            # Disallow any other page until user has been activated
                            #last_path = request.path.rsplit('/', 1)[-1]
                            if (admin_status !='active' and request.path.rsplit('/',1)[-1]=='logout'):
                                # Do nothing. Allow user to logout from activate page.
                                # not elegant way.....but avoids infinite redirection   
                                print('log out')
                            elif (admin_status !='active' and request.path.rsplit('/',1)[-1]!='activate'):
                                return HttpResponseRedirect( reverse('activate') )
                            # Disallow activate page once user has been activated
                            elif (admin_status =='active' and request.path.rsplit('/',1)[-1]=='activate'):
                                return HttpResponseRedirect( reverse('logout') )
                            # get enabled services
                            request.ldap.search(
                                search_base   = settings.LDAP_TREE_SERVICES,
                                search_filter = settings.LDAP_FILTERS_INSTALLED_ENABLED_SERVICES,
                                attributes    = ['ou']
                            )
                            active_apps = [service.ou.value for service in request.ldap.entries]
                            setattr(request, "enabled_services", [service.ou.value for service in request.ldap.entries])
                            # get system reboot info
                            request.ldap.search(
                                search_base   = settings.LDAP_TREE_REBOOT,
                                search_filter = settings.LDAP_FILTERS_REBOOT,
                                attributes    = ['info']
                            )
                            setattr(request, "system_reboot", request.ldap.entries[0].info.value)
                            # get puppet status
                            setattr(request, "status", utils.get_puppet_status(request))

                            """ Get all custom url for active groups, from ldap, if exists """

                            apps_name_domain = []
                            apps_with_domain = utils.get_domain_deps_apps(request)
                            #TODO: merge two lists : from active apps only keep ones that are in apps-with_domain
                            for app in active_apps:
                                if app in apps_with_domain:
                                    try:
                                        request.ldap.search(
                                            search_base   = 'ou=domain,ou=%s,%s' % (app, settings.LDAP_TREE_SERVICES),
                                            search_filter = '(objectClass=organizationalUnit)',
                                            attributes    = ['status']
                                        )
                                        app_domain = request.ldap.entries[0].status.value
                                        if app_domain:                       
                                            apps_name_domain.append(app)
                                            setattr(request, "%s_url" % app, 'https://%s' % app_domain)
                                    except Exception as e:
                                        print("There was a problem retrieving the domain list: %s" % str(e))
                                setattr(request,'apps_name_domain', apps_name_domain)
                    elif ldap['error'] == 'LDAPSocketOpenError':
                        # If socket error, ldap is shut down for some reasons
                        # warn the user accordingly
                        del request.session['user']
                        messages.warning(request, _('Hay problemas de conectividad con la base de datos. '
                                                'Intenta entrar dentro de unos minutos. Disculpa las '
                                                'molestias.'))
                        return HttpResponseRedirect( reverse('login') )
                    else:
                        # If cannot connect to LDAP probably it has to do with LDAP's session
                        # expiration, so redirect to login.
                        """ destroy cookie """
                        del request.session['user']
                        del request.session['enc_pass']
                        messages.error(request, _('Tu sesión ha caducado, por favor introduce tus credenciales nuevamente'))
                        return HttpResponseRedirect( reverse('login') )

                # if the session is closed redirect to login
                except AttributeError:
                    return HttpResponseRedirect( reverse('login') )
            else:
                return HttpResponseRedirect( reverse('login') )


        return self.get_response(request)
