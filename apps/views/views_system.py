import time
import psutil, math, os, re

# django
from django.utils.translation import ugettext_lazy as _
from django import views
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.conf import settings
from django.views.generic.edit import FormView
from django.contrib import messages
from django.contrib.auth import logout as auth_logout

# project
#from .utils import p, get_puppet_status, get_release_info, lock_cpanel, 
from . import utils
from .forms import FqdnForm, SystemConfigForm
from . import debug
from ldap3 import MODIFY_REPLACE, MODIFY_ADD
class Details(views.View):
    """
    System details view.
    """
    graph_width  = 240
    graph_height = 120

    def get_height(self, percent):
        """ Util function to get the height of a value from the related percent. """
        return round(self.graph_height * percent/100)

    def get(self, request):
        vpn_alert = True
        """ Force Users to check security settings"""
        try: 
            request.ldap.search(
                settings.LDAP_TREE_CPANEL,
                '(objectClass=organizationalUnit)',
                attributes=['type', 'info']
            )
            cur_release = request.ldap.entries[0].type.value
            start_release = request.ldap.entries[0].info.value
            #acc_active = request.ldap.entries[0].accountActive.value
            release_number = cur_release.split('_', 1 )
        except Exception as e:
           print("No record fourn") 
        # Make differnce among fresh new install or upgraded VM
        if (start_release == cur_release):
            # If it is a fresh install VPN can not be in use. 
            # Avoid alert if then they install VPN and have not dismissed alert 
            # Security alert is to be shown in any case 
            vpn_alert = False
        """
        organizationalStatus did not exists in previous release, 
        So check separetly to avoid error
        wizardon var is used for showing or not the security alerts, 
        until user checks don't show me again
        """
        try: 
            request.ldap.search(
            settings.LDAP_TREE_CPANEL,
                '(objectClass=organizationalUnit)',
                attributes=['organizationalStatus']
            )
            org_status = request.ldap.entries[0].organizationalStatus.value
            """
            For future release, check when wizard alerts has been removed by user.
            If some VM jumps from 201805 directñy to 202001 
            alert may be different than Upgrading from 201901 to 202001,
            This is just an idea of how it will work. 
            org_status: a number that matches current realease if users checkd 'don't show me again' 
            Use just the number part if we then need to campare
            """
            # users has dismissed for this release
            if org_status==release_number[1]:
                wizard_on = False
            # User has dismissied for previous release
            else:
                wizard_on = True
            # Never dismissed
        except Exception as e:
            wizard_on = True 
        """Handle GET requests."""
        graph_width  = self.graph_width
        graph_height = self.graph_height
        cpu_bars_n = 20
        cpu_barwidth = round(graph_width / cpu_bars_n)
        cpu_bars = [ round(cpu_barwidth * i + 5) for i in range(cpu_bars_n) ]
        # Get system memory stats
        memory    = psutil.virtual_memory()
        ram_size  = self.get_height(memory.percent)
        # Get system CPU stats
        cpu       = psutil.cpu_percent()  # percentage of used cpu
        cpu_count = psutil.cpu_count(logical=False) # number of physical cpus
        cpu_size  = self.get_height(cpu)
        # Get disk usage stats
        disk_usage     = psutil.disk_usage('/')  # gets disk usage of the partition in which the folder is
        disk_size      = self.get_height(disk_usage.percent)
        # DNS for FQDN
        ip             = utils.get_server_ip()
        host           = utils.get_server_host()
        domain_records = utils.get_dns_records(host)
        has_A          = 'A' in domain_records
        record         = domain_records['A'][0] if has_A else None
        records_A      = domain_records['A'] if has_A else [None]
        has_MX         = 'MX' in domain_records
        valid_MX       = host +'.'
        records_MX     = [ mx.to_text().split()[1] for mx in domain_records['MX'] ] if has_MX else [None]
        has_TXT        = 'TXT' in domain_records
        record_SPF     = [ txt for txt in domain_records['TXT'] if 'v=spf' in txt.to_text() ][0] if has_TXT else [None]
        valid_SPF      = "\"v=spf1 a mx ~all\""
        record_DKIM    = utils.get_dkim(host)
        dkim_domain    = "default._domainkey.%s" % host 
        dkim_path      = "/etc/opendkim/keys/%s/default.txt" % host
        required_dkim = utils.get_local_dkim_key(host)
        dkim_matches = utils.compare_dkim_values(record_DKIM,required_dkim)
        """
        if os.path.isfile(dkim_path):
            dkim_file = open(dkim_path).read(1000)
            key = re.search('"p=(.*)"', dkim_file)
            required_dkim = "v=DKIM1; k=rsa; %s" % key.group(0)[1:-1]
        """
        self.release_number = release_number
        return render(request, 'pages/system-details.html', locals())

    @method_decorator(csrf_protect)
    def post(self, request):
        try:
            request.ldap.search(
                settings.LDAP_TREE_CPANEL,
                '(objectClass=organizationalUnit)',
                attributes=['type']
            )
            cur_release = request.ldap.entries[0].type.value
            #acc_active = request.ldap.entries[0].accountActive.value
            release_number = cur_release.split('_', 1 )
        except Exception as e:
           print("No record fourn")

        """Handle POST requests."""
        if request.method=='POST' and 'wizardconfirm' in request.POST:	
            try: 
                request.ldap.modify(
                settings.LDAP_TREE_CPANEL, {
                    'organizationalStatus' : [(MODIFY_REPLACE,release_number[1])],
                    }
                )
            except Exception as e:
                request.ldap.modify(
                settings.LDAP_TREE_CPANEL, {
                    'organizationalStatus' : [(MODIFY_ADD, release_number[1])],
                }
                )

        return HttpResponseRedirect( reverse('system-details') )

@csrf_protect
@require_POST
def get_cpu_usage(request):
    """
    View to get the CPU usage of the host machine.
    """
    cpu = psutil.cpu_percent()
    return HttpResponse(cpu, content_type="application/json")




class Reboot(views.View):
    """
    View to reboot the system.
    """

    def get(self, request):
        """Handle GET requests."""

        return render(request, 'pages/reboot.html')

    @method_decorator(csrf_protect)
    def post(self, request):
        """Handle POST requests."""

        try:
            request.ldap.modify(
                settings.LDAP_TREE_REBOOT, {
                    'info'   : [(MODIFY_REPLACE, 'ready')],
                    'status' : [(MODIFY_REPLACE, 'locked')],
                }
            )
            utils.lock_cpanel(request)
        except Exception as e:
            utils.p("Reboot view", "There was a problem retrieving updating cpanel/reboot attributes", e)

        return HttpResponseRedirect( reverse('logout') )


class Update(views.View):
    """
    View to update the system.
    """

    def get(self, request):
        """Handle GET requests."""

        context = {}
        try: 
            request.ldap.search(
                settings.LDAP_TREE_CPANEL,
                '(objectClass=*)',
                attributes=['type']
            )
            cur_release = request.ldap.entries[0].type.value
            context['current_release'] = cur_release.replace("_", " ")
        except Exception as e:
            context['current_release'] = None

        if settings.NO_API:
            puppet_status = debug.STATUS
        else:
            puppet_status = utils.get_puppet_status(request)
        context['status'] = puppet_status.get('puppetstatus')
        if context['status'] == 'pending' or context['status'] == 'error':
            context['status'] = context['status']
        elif context['status'] == 'ready':
            if settings.NO_API:
                release_info = debug.RELEASE
            else:
                updates = 'updates'
                release_info = utils.get_release_info(request, updates)
                # TODO: check if there is a newer release , return the release info.
                if release_info:
                    context['release']     = release_info['release']
                    context['description'] = release_info['description']
                else:
                    context['status'] = 'last-update'
        else:
            context['status'] = 'no-updates'
        return render(request, 'pages/update.html', context)

    @method_decorator(csrf_protect)
    def post(self, request):
        """Handle POST requests."""

        try:
            release = request.POST.get("release")
            attr = {
                'status' : [(MODIFY_REPLACE, 'locked')],
            }
            if (release and release != 'pending'):
                attr['type'] = [(MODIFY_REPLACE, release)]

            request.ldap.modify(settings.LDAP_TREE_CPANEL, attr)
        except Exception as e:
            utils.p("Update view", "There was a problem updating cpanel attributes", e)

        return HttpResponseRedirect( reverse('logout') )

class Fqdn(FormView):
    """ 
    This Form allows to change the FQDN of the server 
    The hostname must be preserverd, but the assoicated domain can be changed.
    The process needs following actions and checks:
    - check that domain is not in ldap or in use by other applications
    - check dns entries for new desired fqdn
    - save the old fqdn value in old_domain attribute in ldap (used by puppet)
    - activate all deactivate groups (if there are any)
    - Lock cpanel and destroy session
    """

    template_name = 'pages/fqdn.html'
    form_class    = FqdnForm
    domains       = {}
    puppet_status = {}

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context               = super(Fqdn, self).get_context_data(**kwargs)
        context['fqdn'] = self.fqdn
        context['servername'] = self.servername
        # get the server domain : remove servername from fqdn
        # didn't find a python function equivalent to hostname -d 
        context['server_domain'] = self.server_domain
        context['display_form'] = 'form' in kwargs
        context['ip'] = utils.get_server_ip()
        #puppet_status = utils.get_puppet_status(self.request)
        #context['maintenance'] = puppet_status['puppetstatus']
        return context

    def get_form(self):
        """Return an instance of the form to be used in this view."""
        self.fqdn = utils.get_server_host()
        self.servername = utils.get_server_hostname()
        replacing_string = "%s." % self.servername
        self.server_domain = self.fqdn.replace(replacing_string, "", 1)
        domains = utils.get_existing_domains(self.request.ldap)
        domain_list = [ domain.vd.value for domain in domains ]
        domains_in_use = utils.domain_is_in_use(self.request.ldap)
        domains_used_list = [ domain.status.value for domain in domains_in_use]
        return FqdnForm( domain_list=domain_list, domains_used_list=domains_used_list,servername=self.servername, **self.get_form_kwargs())

    def form_valid(self, form):
        newdomain = form['name'].value()
        custom_logs = 'true' if form['log_server'].value() == True else 'false'
        #TODO: if we use .lower in bool_kdap() we can just use
        #custom_logs = form['log_server'].value()
        base_dn = "ou=conf,%s" %  settings.LDAP_TREE_CPANEL
        errors=False
        """If the form is valid, update values in ldap """

        try:
            dn = "ou=fqdn_domain,%s" % base_dn 
            self.request.ldap.modify(dn, {
                'status' : [(MODIFY_REPLACE, newdomain)]
            })
            # update old_domain value
            # normally  object ou=fqdn_domain_old does not exixts, create it 
            # else update status value
            dn = "ou=fqdn_domain_old,%s" % base_dn
            try:
                self.request.ldap.add(dn, ['organizationalUnit', 'metaInfo', 'top'], {
                    'status'            : self.server_domain,
                })
            except Exception as e:
                if e.result == 68: 
                    self.request.ldap.modify(dn, {
                    'status' : [(MODIFY_REPLACE, self.server_domain)]
                })

            # Update custom logs settings
            dn = 'ou=logmail_custom,%s' % base_dn
            self.request.ldap.modify(dn, {
                'status' : [(MODIFY_REPLACE, custom_logs )]
            })
            # Activate all deactivated groups
          
            self.request.ldap.search(
                settings.LDAP_TREE_BASE,
                settings.LDAP_FILTERS_INSTALLED_DISABLED_SERVICES,
                attributes=['ou']
            )
            disabled_service_names = [ service.ou.value for service in self.request.ldap.entries ]
            if (disabled_service_names):
                for service in disabled_service_names:
                    dn = 'ou=%s,%s' % (service, settings.LDAP_TREE_SERVICES)
                    self.request.ldap.modify(dn, {
                        'type'   : [(MODIFY_REPLACE, 'available')],
                        'status' : [(MODIFY_REPLACE, 'enabled')]
                    })

        except RuntimeError as re:
            print("There was a problem updating ldap data")
            print(re)
            errors=True

        except Exception as e:
            messages.error(self.request, _('Ha habido un error y no se puede continuar con la operación. '
                                           'Si el problema persiste contacta '
                                           'con los administrador-s'))

            print("There was a problem updating")
            print(e)
            errors=True
        #if everything is ok, lock cpanel
        # Avoid unlog user if something went wrong
        if not errors:
            # set ou=customfqdn,ou=cpanel, status as locked
            dn = "ou=customfqdn,%s" %  settings.LDAP_TREE_CPANEL 
            self.request.ldap.modify(dn, {
                'status' : [(MODIFY_REPLACE, 'locked')]
            })

            # time.sleep(t) because local cpanel must run befor puppet master
            time.sleep(1)
            # lock cpanel ad log out user
            utils.lock_cpanel(self.request) 
            # Manually destroy session
            username = self.request.session['user']
            del self.request.session['user']
            del self.request.session['enc_pass']
            # Destroy cookies
            next_page = 'changesystem'
            path = settings.FORCE_SCRIPT_NAME
            response  = HttpResponseRedirect(reverse(next_page), locals)
            response.delete_cookie('s')
            response.set_cookie('oldDomain', self.servername + '.' + self.server_domain)
            response.set_cookie('newDomain', self.servername + '.' + newdomain)
            utils.p("Logout view", "this is username: ", username)
            # redirect to settings.LOGOUT_REDIRECT_URL
            auth_logout(self.request)
            return response
        else: 
            return super(Fqdn, self).form_valid(form)
        
class NewFqdnSet(views.View):
    def get(self, request):
        # TODO: if user is logged in redirect to cpanel homepage (details)
        path = settings.FORCE_SCRIPT_NAME 
        return render(request, 'registration/fqdn-change.html', locals())

class Clean_system(views.View):
    """ 
    View to reboot the system.
    """
    def get(self, request):
        try:
            request.ldap.search(
	        settings.LDAP_TREE_BASE,
                settings.LDAP_FILTERS_INSTALLED_ENABLED_SERVICES,
                attributes=['ou']
            )
            # Get all enables service to check what can be removes
            enabled_service_names = [ service.ou.value for service in request.ldap.entries ]
            optionals = []	
            """ DOCKER """
            if 'docker' in enabled_service_names:
                docker_clean_message = _("Se eliminarán las imágenes de docker que no están referenciadas ni usadas por ningún contenedor." 
                                         "Si has instalado alguna imagen de docker de forma manual, sin utilizar el panel de control, asegúrate de no tender ninguna que no esté actualmente en uso y que quieras conservar.")
                optionals.append(docker_clean_message)
        except Exception as e:
            utils.p("Clean view", "No apps installed yet. Nothing else to clean", e)

        return render(request, 'pages/clean.html', locals())

    @method_decorator(csrf_protect)
    def post(self, request):
        errors=False
        """Handle POST requests."""
        try:
            request.ldap.search(
                settings.LDAP_TREE_CLEAN,
                settings.LDAP_FILTERS_CLEAN,
                attributes=['cn']
            )
            # if clean tree already exist in ldap update it
            if self.request.ldap.entries:
                try:
                    request.ldap.modify(
                        settings.LDAP_TREE_CLEAN, {
                            'info'   : [(MODIFY_REPLACE, 'ready')],
                            'status' : [(MODIFY_REPLACE, 'locked')],
                        }
                    )
                    messages.success(request, _('Operación guardada con éxito' ))
                except Exception as e:
                    messages.error(request, _('Se ha producido un error' ))
                    utils.p("Clean view", "Error updating clean entry in ldap", e)
        except Exception as e:
            utils.p("Clean view", "No clean entry found in ldap", e)
            errors=True

        if errors:
            try:
                # if clean tree does not exixts in ldap create it with needed value 
                request.ldap.add(settings.LDAP_TREE_CLEAN, [
                    'organizationalUnit',
                    'metaInfo',
                    'extensibleObject'
                    ],
                    {'info': 'ready', 'status': 'locked'})

                messages.success(request, _('Operación guardada con éxito' ))
            except Exception as e:
                messages.error(request, _('Se ha producido un error' ))
                utils.p("Clean view", "Error creating  clean entry in ldap", e)

        return HttpResponseRedirect( reverse('system-details') )

class SystemConfig(FormView):
    template_name = 'pages/system-config.html'
    form_class    = SystemConfigForm

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):
        context = super(SystemConfig, self).get_context_data(**kwargs)
        context['show_modal'] = 'form' in kwargs
        return context

    def get_form(self):
        """Return an instance of the form to be used in this view."""
        """ This form includes several settings all at once, divided 
            Sections: SSH, email server, Apache
        """
        # get ssh config from ldap
        ssh_dn = "ou=sshd,ou=conf,%s" %  settings.LDAP_TREE_CPANEL
        try:
            self.request.ldap.search(
                ssh_dn,
                '(objectClass=extensibleObject)',
                attributes=['ipServicePort', 'ipServiceProtocol']
            )
            sshdata = self.request.ldap.entries[0]
            ssh_port = sshdata.ipServicePort.value
            ssh_protocol = utils.ldap_val(sshdata.ipServiceProtocol.value)
        except Exception as e:
            utils.p("✕ view_system.py", "No results foun :", e)
            ssh_port = int(22) 
            # Protocol:
            #False normal login setting with password allowed for SSH
            #True Only ssh key login allowed
            ssh_protocol = False
        args =  {
            'protocol_sshd': ssh_protocol,
            'ssh_port': ssh_port,
            }
              
        # Check TLS settings for apache and email
        services_tls = ["apache", "email"]
        for service in services_tls:
            dn = "ou=%s,ou=conf,%s" % (service, settings.LDAP_TREE_CPANEL)
            try: 
                self.request.ldap.search(
                    dn,
                    '(objectClass=extensibleObject)',
                    attributes=['ipServiceProtocol'],
                )
                data = self.request.ldap.entries[0]
                protocol =  utils.ldap_val(data.ipServiceProtocol.value)
                args.update({ 'protocol_%s' % service : protocol })
            except Exception as e:
                """
                If no entry found, set to the default settings
                as previous releases had not this settings and 
                te one applied is 1
                1- Insecure
                2- Moderate
                3- Secure
                """
                utils.p("✕ view_system.py", "No results found for service:", e)
                protocol = 3
                args.update({ 'protocol_%s' % service : protocol })

        kwargs = self.get_form_kwargs()
        kwargs['initial'] =  args 
        return SystemConfigForm(**kwargs)

    def form_valid(self,form, *args, **kwargs):
        #ssh_port        =int(form['ssh_port'].value())
        #ssh_key_required=form['prtocol_sshd'].value()  
        #ssh_dn = "ou=sshd,ou=conf,%s" %  settings.LDAP_TREE_CPANEL 
        context = self.get_context_data(**kwargs)
        context['show_modal'] = True
        print('CONTEXT ', context)
        if self.request.method=='POST' and 'writeldap' in self.request.POST:
            services_tls = ["apache", "email", "sshd"]
            errors=False	
            for service in services_tls:
                args = {}
                dn = "ou=%s,ou=conf,%s" % (service, settings.LDAP_TREE_CPANEL)
                # Check if entries has been created yet	
                fieldname = "protocol_%s" % service
                try:
                    self.request.ldap.search(
                        dn,
                        '(objectClass=extensibleObject)',
                        attributes=['ou'],
                    )
                    self.request.ldap.entries	
                    # If enris already exists, update in ldap
                    try:
                        args.update({
                             'ipServiceProtocol' : [( MODIFY_REPLACE, form[fieldname].value() )],
                             })
                        if service == 'sshd':
                            args.update({ 'ipServicePort' : [( MODIFY_REPLACE, int(form['ssh_port'].value()))]})
                        self.request.ldap.modify(dn, args)
                    except Exception as e:
                        utils.p("✕ view_system.py", "There was a problem updating ssh service :%s" % service,  e)
                        messages.error(self.request, _('Ha habido un error. No se ha podido actualizar la configuración'))
                        errors=True	
                except Exception as e:
                    """
                    If no entry found, set to the default settings
                    For TLS ipServiceProtocol values are
                    1- Insecure
                    2- Moderate
                    3- Secure
                    For sshd ipServiceProtocol vlaues are
                    1- Allow ssh login with password
                    0- Disallow
                    ipServiceProtocol=1 is the default setting if
                    none custom config has been entered yet
                    """
                    utils.p("✕ view_system.py", "No results found for service:", e)
                    try:	
                        args.update({'ipServiceProtocol' : form[fieldname].value()})
                        if service == 'sshd':
                            args.update({'ipServicePort' : int(form['ssh_port'].value())})
                        self.request.ldap.add(dn, [
                            'organizationalUnit',
                            'metaInfo',
                            'extensibleObject'
                            ],
                            args,
                        )
                    except Exception as e:
                        utils.p("✕ view_system.py", "There was a problem creating the sshd entry  :",  e)
                        messages.error(self.request, _('Ha habido un error. No se ha podido actualizar la configuración'))
                        errors=True	
            if not errors:
                #If all updates where successfull lock the panel and logout user
                utils.lock_cpanel(self.request)
                return HttpResponseRedirect( reverse('logout') )
        #return super(SystemConfig, self).form_valid(form)
        return self.render_to_response(context)
