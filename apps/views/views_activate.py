# python
import re
# django
from django.utils.translation import ugettext_lazy as _
from django import views
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.urls import reverse_lazy, reverse
from django.contrib import messages
from django.contrib.auth.views import LogoutView as logout
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.contrib.auth import logout as auth_logout

# contrib
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA, MODIFY_ADD 
from ldap3.utils.hashed import hashed
from ldap3.core.exceptions import LDAPSocketOpenError
# project
from django.conf import settings
from . import utils, forms


class Activate(FormView):
    form_class    = forms.ActivateForm
    template_name = 'pages/activate.html'
    def get_success_url(self):
        """ Redirection url if form is valid."""
        return reverse('logout')
    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context               = super(Activate, self).get_context_data(**kwargs)
        context['username'] = self.username
        context['sudousername'] = self.sudousername
        return context


    def get_form(self):
        """ Return an instance of the form to be used in this view."""
        # Get ldap admin user data
        """
        f= open("/etc/facter/facts.d/classifier.yaml","r") 
        facts = f.read() 
        f.close()
        print(facts)

        """
        self.username = self.request.user.username
        userfilter = "(&(objectClass=extensibleObject)(cn=%s))" % self.username
        self.request.ldap.search(
            settings.LDAP_TREE_BASE,
            userfilter,
            attributes=['email']
        )
        user  = self.request.ldap.entries[0]
        #print(user)
        # get sudo user data
        sudouserfilter = "(&(objectClass=person)(uid=*)(gidnumber=27))"
        self.request.ldap.search(
            settings.LDAP_TREE_BASE,
            sudouserfilter,
            attributes=['uid']
        )
        self.sudousername  = self.request.ldap.entries[0]['uid']
        kwargs = self.get_form_kwargs()
        kwargs['initial'] = {
            'sudousername' : self.sudousername,
            'username' : self.username,
            'email'    : user.email.value,
        }
        return forms.ActivateForm(
           # pwd  = utils.dec_pwd(self.request),
            **kwargs
        )

    def form_valid(self, form):
        """ Actions to be performed if the form is valid. """

        username = self.request.user.username
        sudousername = self.sudousername
        bad_credentials = _("Las credenciales son incorrectas.")
        password = form["current_password"].value()
        # set fields to be updated
        fields = {}
        sufields = {}
        attr = {}

        # Use user input password to connect to ldap
        ldap = utils.connect_ldap(username, password)
        if ldap['connection']:
            # Activate grous in ldap from facts
            regex = r"default_groups[^\[]*\[(.*?)]"
            f= open("/etc/facter/facts.d/classifier.yaml","r") 
            facts = f.read() 
            matches = re.finditer(regex, facts, re.MULTILINE)
            for match in matches:
                allgroups = match.group(1)
                print(allgroups)
                allgroups = allgroups.split(',')
                for singroup in allgroups:
                    singroup = singroup.strip()
                    print(singroup)
                    dngroup = "ou=%s,%s" % (singroup, settings.LDAP_TREE_SERVICES)
                    # Update groups group entry in ldap- set ti enabled if some group is already enabled from bootstrap
                    try:
                        self.request.ldap.search(
                        dngroup,
                        "(&(objectClass=organizationalUnit)(status=*))",
                        )
                        if self.request.ldap.entries:
                            attr['status'] = [(MODIFY_REPLACE, "enabled")]
                            attr['type'] = [(MODIFY_REPLACE, "available")]
                            self.request.ldap.modify(dngroup, attr)
                    except Exception as e:
                        utils.p("✕ activate", "There was a problem updating the group: %s" % singroup,e) 


            f.close()
            # Update sudo user password and ctivate services 
            if 'sudopassword' in form.fields and form['sudopassword'].value():
                sudopwd = form['sudopassword'].value()
                services = ['sshd', 'apache', 'cron', 'sudo']
                sufields['userpassword'] = [(MODIFY_REPLACE, hashed(HASHED_SALTED_SHA, sudopwd))]
                sufields['authorizedservice'] = [(MODIFY_REPLACE, services )]

            # build dn of the sudo user and update password
            dnsuperuser = "uid=%s,%s" % (sudousername, settings.LDAP_TREE_USERS)
            self.request.ldap.modify(dnsuperuser, sufields)
            # set the status of the ldap admin user to active .
            # Activation is required only on first login 
            # Update ldap admin user data  : password and email 
            if 'email' in form.fields and form['email'].value():
                fields['email'] = [(MODIFY_REPLACE, form['email'].value())]
            if 'password' in form.fields and form['password'].value():
                pwd = form['password'].value()
                fields['userpassword'] = [(MODIFY_REPLACE, hashed(HASHED_SALTED_SHA, pwd))]
                fields['status'] = [(MODIFY_REPLACE, 'active')]
            dnuser     = "cn=%s,%s" % (username, settings.LDAP_TREE_BASE)
            # modify user fields
            #print(dn, fields)
            self.request.ldap.modify(dnuser, fields)

            if pwd:
                messages.success(self.request, _('Activación realizada con éxito. '
                                                 'Utiliza la nueva contraseña para entrar.'))
            else:
                messages.success(self.request, _('Activación realizada con éxito.'))
            """ delete cookie - We use user input passord """
            response  = HttpResponseRedirect(reverse('logout'))
            response.delete_cookie('s')
            return response
        elif ldap['error'] == 'LDAPSocketOpenError':
            # If socket error, ldap is shut down for some reasons
            # warn the user accordingly
            messages.error(self.request, _('Hay problemas de conectividad con la base de datos. '
                                      'Intenta entrar dentro de unos minutos. Disculpa las '
                                      'molestias.'))
            #return HttpResponseRedirect( reverse('login') )
        elif ldap['error'] == 'LDAPInvalidCredentialsResult':
            messages.error(self.request, _('Las credenciales son incorrectas. ¿Las has escrito correctamente?'))
            #return HttpResponseRedirect( reverse('login') )

