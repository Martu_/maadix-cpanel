/**
 * common.js
 * Common scripts to be used throughout the site
 */

function get_installation_path(){
  //var path = '';
  var path = '/cpanel';
  return path;
}

function toggleCollapsible(trigger)
{
    var target_id     = trigger.getAttribute('data-toggle');
    var target        = document.querySelector('#'+target_id);
    var excluded      = trigger.getAttribute('data-excluded');
    var is_collapsed  = target.classList.contains('collapsed');
    if(excluded){
        [...document.querySelectorAll('.' + excluded)].forEach(
            function(trigger){
                trigger.classList.add('collapsed')
            }
        );
    }
    if(is_collapsed)
        target.classList.remove('collapsed');
    else
        target.classList.add('collapsed');
}

/**
 *  Returns the value of a cookie
 *  @see {@link https://docs.djangoproject.com/en/2.0/ref/csrf/}
 *  @param {str} name - Name of the cookie
 *  @returns {str}
 */
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

/**
 *  Makes AJAX POST request
 *  @param {str} dn - DN of the entry
 */
function post(url, data)
{
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", url);
    xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == XMLHttpRequest.DONE)
            console.log( xmlhttp.responseText );
        if(xmlhttp.status == 200)
            window.location.reload();
    };
    var csrftoken = getCookie('csrfcpaneltoken');
    xmlhttp.setRequestHeader("X-CSRFToken", csrftoken);
    xmlhttp.send(data);
}

/**
 *  Makes AJAX POST request
 *  @param {str} dn - DN of the entry
 */
function postNoReload(url, data, cb)
{
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", url, false);
    xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == XMLHttpRequest.DONE && xmlhttp.status == 200)
            if( typeof cb === 'function' )
              cb(xmlhttp.responseText);
            //return xmlhttp.responseText;
    };  
    var csrftoken = getCookie('csrfcpaneltoken');
    xmlhttp.setRequestHeader("X-CSRFToken", csrftoken);
    xmlhttp.send(data);
    return xmlhttp.responseText;
}

/**
 * @param {String} HTML representing a single element
 * @return {Element}
 */
function htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
}

/**
 *  Cancels modal
 *  @param {str} modal - BEM Class modifier that represents target modal
 */
function removeModal(modal){
    var modal = document.querySelector('.modal--' + modal);
    if(modal)
      document.body.removeChild(modal);
}

/**
 *  Launch modal
 *  @param {str} target - Markup query that represents the modal
 */
function launchModal(target){
    var modal = document.querySelector(target);
    modal.style.display = 'block';
}

/**
 *  Launch modal
 *  @param {str} target - Markup query that represents the modal
 */
function dismiss(target){
    var target = document.querySelector(target);
    target.style.display = 'none';
}

/**
 *  Add event listeners when DOM is completely loaded
 *  equivalent to $(document).ready()
 */
document.addEventListener("DOMContentLoaded", function()
{
    [...document.querySelectorAll('[data-toggle]')].forEach(
        function(trigger){
            trigger.addEventListener('click', function(){
                toggleCollapsible(trigger);
            });
        }
    );

    [...document.querySelectorAll('[data-set-delete-dn]')].forEach(
        function(trigger){
            trigger.addEventListener('click', function(){
                var submit = document.querySelector('[name=dn]');
                submit.value = trigger.dataset.setDeleteDn
                var text   = document.querySelector('.modal__text--delete');
                text.innerHTML = trigger.dataset.setDeleteText
            });
        }
    );

    [...document.querySelectorAll('[data-set-notice-list]')].forEach(
        function(trigger){
            trigger.addEventListener('click', function(){
                var text   = document.querySelector('.modal__text--notice');
                text.innerHTML = trigger.dataset.setNoticeText
                console.log('tect ' + trigger.dataset.setNoticeText);
                var list   = document.querySelector('.modal__text--list');
                list.innerHTML = trigger.dataset.setNoticeList
            });
        }
    ); 
    [...document.querySelectorAll('[data-launch-modal]')].forEach(
        function(trigger){
            trigger.addEventListener('click', function(){
                launchModal(trigger.dataset.launchModal);
            });
        }
    );

    [...document.querySelectorAll('[data-dismiss]')].forEach(
        function(trigger){
            trigger.addEventListener('click', function(){
                dismiss(trigger.dataset.dismiss);
            });
        }
    );

    [...document.querySelectorAll('[data-link-display]')].forEach(
        function(trigger){
            var target = document.querySelector('#' + trigger.dataset.linkDisplay)
            var field  = target.parentNode;
            if(!trigger.checked)
                field.classList.add('collapsed');
            trigger.addEventListener('change', function(){
                if(trigger.checked){
                    field.classList.remove('collapsed');
                } else {
                    field.classList.add('collapsed');
                }
            });
        }
    );
    var hamburger = document.getElementsByClassName("hamburguer")[0];
    if (typeof(hamburger) != 'undefined' && hamburger != null) {
      document.querySelector('.hamburguer').addEventListener('click', function(){
          document.body.classList.toggle('navigation-open');
      });
    }
    var closemessage = document.getElementsByClassName("closeMessage")[0];
    if (typeof(closemessage) != 'undefined' && closemessage!= null) {
      document.querySelector('.closeMessage').addEventListener('click', function(){
          target = document.querySelector(".messages-list__msg--warning");
          target.classList.add('closing');

      });
    }
});
